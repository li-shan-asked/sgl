/* source/widget/sgl_appstart.c
 *
 * MIT License
 *
 * Copyright(c) 2023-present All contributors of SGL  
 *  
 * Document reference link: www.sgl-io.cn
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "./sgl_appstart.h"
#include "../core/sgl_obj.h"
#include "../core/sgl_page.h"
#include "../core/sgl_device.h"
#include "../core/sgl_task.h"
#include "../stdlib/sgl_mem.h"
#include "../stdlib/sgl_string.h"
#include "../draw/sgl_draw.h"
#include "../draw/sgl_draw_rect.h"
#include "../draw/sgl_draw_font.h"
#include "../draw/sgl_draw_circle.h"
#include "../draw/sgl_draw_icon.h"
#include "../draw/sgl_draw_arc.h"


static void sgl_appstart_event_cb(sgl_obj_t *obj);


int sgl_appstart_init(sgl_appstart_t *appstart, sgl_obj_t* parent)
{
    sgl_obj_t* obj;
    if(parent == NULL) {
        return -1;
    }
    obj = &appstart->obj;
    obj->event_data = NULL;
    obj->ev_stat = SGL_EVENT_FORCE_DRAW;
    obj->parent = parent;
    obj->dirty = 1;
    obj->selected = 0;
    obj->hide = 0;
    obj->draw_cb = sgl_appstart_event_cb;
    sgl_page_add_obj(parent, obj);
    return 0;
}

sgl_obj_t* sgl_appstart_create(sgl_obj_t* parent)
{
    int ret;
    sgl_appstart_t *appstart;
    if(parent == NULL) {
        return NULL;
    }
    appstart = (sgl_appstart_t *)sgl_malloc(sizeof(sgl_appstart_t));
    if(appstart == NULL)
        return NULL;
    
    ret = sgl_appstart_init(appstart, parent);
    if(ret) {
        return NULL;
    }
    return  &appstart->obj;
}


static void sgl_appstart_draw(sgl_obj_t* obj)
{
    sgl_surf_t *surf = sgl_get_active_surf(obj);
    sgl_img_t *bg_img = sgl_obj_get_bg_img(obj->parent);
    sgl_appstart_t *appstart = container_of(obj, sgl_appstart_t, obj);
    sgl_widget_draw_rect(rect);
    sgl_widget_buffer_valid(obj);

    //if bg_img is NULL, do nothing
    if(obj->bg_img == NULL) {
        return;
    }
    if(bg_img == NULL) {
        sgl_draw_obj_buffer_clear(surf, obj, obj->parent->style->body_color);
        if(obj->ev_stat == SGL_EVENT_FORCE_DRAW_CLEAR) {
            return;
        }
        if(!obj->alpha) {
            if(obj->bg_img == NULL) {
                sgl_draw_round_rect_solid(surf,
                                rect,
                                obj->style->radius,
                                obj->style->body_color, obj->parent->style->body_color
                                );
            }
            else {
                sgl_draw_round_rect_img(surf,
                                rect,
                                obj->style->radius,
                                obj->parent->style->body_color, obj->bg_img
                                );
            }

        }
        else {
            if(obj->bg_img == NULL) {
                sgl_draw_round_rect_transp_on_bg(surf,
                                rect,
                                obj->style->radius,
                                obj->style->body_color, obj->alpha
                                );
            }
            else {
                sgl_draw_round_rect_img_transp_on_bg(surf,
                                rect,
                                obj->style->radius,
                                obj->bg_img, obj->alpha
                                );
            }
        }
    }
    else {
        sgl_draw_obj_buffer_pick_img(surf, obj, obj->parent->bg_img);
        if(obj->ev_stat == SGL_EVENT_FORCE_DRAW_CLEAR) {
            return;
        }
        if(!obj->alpha) {
            if(obj->bg_img == NULL) {
                sgl_draw_round_rect_solid_on_bg(surf,
                                rect,
                                obj->style->radius,
                                obj->style->body_color
                                );
            }
            else {
                sgl_draw_round_rect_img_on_bg(surf,
                                rect,
                                obj->style->radius,
                                obj->bg_img
                                );
            }

        }
        else {
            if(obj->bg_img == NULL) {
                sgl_draw_round_rect_transp_on_bg(surf,
                                rect,
                                obj->style->radius,
                                obj->style->body_color, obj->alpha
                                );
            }
            else {
                sgl_draw_round_rect_img_transp_on_bg(surf,
                                rect,
                                obj->style->radius,
                                obj->bg_img, obj->alpha
                                );
            }
        }
    }
    sgl_widget_draw_buffer_flush(obj, surf);
    //ensure text and font is NULL
    if(appstart->font != NULL && appstart->text != NULL) {
        sgl_size_t _size;
        sgl_pos_t pos;
        _size.w = sgl_text_width(appstart->font, appstart->text);
        _size.h = sgl_text_height(appstart->font);

        pos.x = obj->pos.x + ((obj->size.w - _size.w) / 2);
        pos.y = obj->pos.y + (obj->size.h + 1);

#if SGL_CONFIG_FRAMEBUFFER_MMAP

        sgl_obj_t tmp_obj = {.pos = pos, .size = _size };
    
        if(bg_img == NULL){        
            sgl_draw_obj_buffer_clear(surf, &tmp_obj, obj->parent->style->body_color);
            sgl_draw_font_string(surf, pos.x, pos.y, appstart->text, obj->style->text_color, obj->parent->style->body_color, appstart->font); 
        }
        else {
            sgl_draw_obj_buffer_pick_img(surf, &tmp_obj, bg_img);
            if(!obj->alpha) {
                sgl_draw_font_string_on_bg(surf, pos.x, pos.y, appstart->text, obj->style->text_color, appstart->font);   
            }
            else {
                sgl_draw_font_string_transp_on_bg(surf, pos.x, pos.y, appstart->text, obj->style->text_color, appstart->font, obj->alpha);
            }
        }
#else
        sgl_obj_t tmp_obj = {.pos = pos, .size = _size };
        sgl_obj_t *tmp_obj_p = &tmp_obj;
        
        sgl_set_active_surf_size(_size.w, _size.h);
    
        if(bg_img == NULL){        
            sgl_draw_obj_buffer_clear(surf, tmp_obj_p, obj->parent->style->body_color);
            sgl_draw_font_string(surf, 0, 0, appstart->text, obj->style->text_color, obj->parent->style->body_color, appstart->font); 
        }
        else {
            sgl_draw_obj_buffer_pick_img(surf, tmp_obj_p, bg_img);
            if(!obj->alpha) {
                sgl_draw_font_string_on_bg(surf, 0, 0, appstart->text, obj->style->text_color, appstart->font);   
            }
            else {
                sgl_draw_font_string_transp_on_bg(surf, 0, 0, appstart->text, obj->style->text_color, appstart->font, obj->alpha);
            }
        }
        sgl_draw_buffer_flush(surf, pos.x, pos.y);
#endif
    }
}


static void sgl_appstart_event_cb(sgl_obj_t *obj)
{
    if(obj->ev_stat == SGL_EVENT_PRESSED) {
        sgl_obj_event_cb(obj);
        sgl_appstart_draw(obj);
    }
    else if(obj->ev_stat == SGL_EVENT_FORCE_DRAW) {
        sgl_appstart_draw(obj);
    }
}


void sgl_appstart_set_font(sgl_obj_t *obj, sgl_font_t *font)
{
    sgl_appstart_t *appstart = container_of(obj, sgl_appstart_t, obj);
    appstart->font = font;
}

void sgl_appstart_set_text(sgl_obj_t *obj, const char *text)
{
    sgl_appstart_t *appstart = container_of(obj, sgl_appstart_t, obj);
    appstart->text = text;
}
