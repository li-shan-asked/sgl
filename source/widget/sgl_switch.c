/* source/widget/sgl_switch.c
 *
 * MIT License
 *
 * Copyright(c) 2023-present All contributors of SGL
 *  
 * Document reference link: www.sgl-io.cn
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "./sgl_switch.h"
#include "../core/sgl_obj.h"
#include "../core/sgl_page.h"
#include "../core/sgl_device.h"
#include "../core/sgl_task.h"
#include "../stdlib/sgl_mem.h"
#include "../stdlib/sgl_string.h"
#include "../draw/sgl_draw.h"
#include "../draw/sgl_draw_rect.h"
#include "../draw/sgl_draw_font.h"
#include "../draw/sgl_draw_circle.h"
#include "../draw/sgl_draw_icon.h"
#include "../core/sgl_anim.h"

static void sgl_switch_event_cb(sgl_obj_t *obj);

#define SWITCH_BORDER  3


static void sgl_switch_draw(sgl_obj_t* obj);

typedef struct switch_anim {
    sgl_anim_t anim;
    int16_t pos;

}switch_anim_t;

void switch_anim_cb(sgl_anim_t *anim, void *data)
{
    switch_anim_t *sw_anim = container_of(anim, switch_anim_t, anim);
    if(sgl_switch_get_status(anim->obj)) {
        sw_anim->pos += SWITCH_BORDER;
        if(sw_anim->pos >= (anim->obj->size.w - anim->obj->size.h - SWITCH_BORDER)) {
            sgl_anim_inactive(anim);
            return;
        }
    }
    else {
        sw_anim->pos -= SWITCH_BORDER;
        if(sw_anim->pos < 0) {
            sgl_anim_inactive(anim);
            return;
        }
    }
    sgl_switch_draw(anim->obj);
}


int sgl_switch_init(sgl_switch_t *sw_btn, sgl_obj_t* parent, bool status)
{
    sgl_obj_t* obj;
    if(parent == NULL) {
        return -1;
    }
    sw_btn->status = status;
    sw_btn->private_data = NULL;
    obj = &sw_btn->obj;
    obj->event_data = NULL;
    obj->ev_stat = SGL_EVENT_FORCE_DRAW;
    obj->parent = parent;
    obj->dirty = 1;
    obj->selected = 0;
    obj->hide = 0;
    obj->draw_cb = sgl_switch_event_cb;
    sgl_page_add_obj(parent, obj);
    return 0;
}

sgl_obj_t* sgl_switch_create(sgl_obj_t* parent, bool status)
{
    int ret;
    sgl_switch_t *sw_btn;
    if(parent == NULL) {
        return NULL;
    }
    sw_btn = (sgl_switch_t *)sgl_malloc(sizeof(sgl_switch_t));
    if(sw_btn == NULL)
        return NULL;
    
    ret = sgl_switch_init(sw_btn, parent, status);
    if(ret) {
        return NULL;
    }
    return  &sw_btn->obj;
}


static void sgl_switch_draw(sgl_obj_t* obj)
{
    sgl_widget_draw_rect(rect);
    sgl_widget_buffer_valid(obj);
    switch_anim_t *sw_anim;
    sgl_switch_t *sw_btn = container_of(obj, sgl_switch_t, obj);
    sgl_surf_t *surf = sgl_get_active_surf(obj);
    sgl_color_t dot_color;
    int toggle_w = obj->size.h - 2 * SWITCH_BORDER;
    sgl_widget_buffer_valid(obj);
    sgl_img_t *bg_img = sgl_obj_get_bg_img(obj->parent);
    sgl_img_t *obj_img = obj->bg_img;
    int16_t anim_pos;
    int16_t rect_radius = sgl_min(obj->style->radius, obj->size.h / 2);

    if(bg_img == NULL)
        sgl_draw_obj_buffer_clear(surf, obj, obj->parent->style->body_color);
    else
        sgl_draw_obj_buffer_pick_img(surf, obj, obj->parent->bg_img);
    if(obj->hide) return;

    if(sw_btn->status) {
        if(sw_btn->private_data != NULL) {
            sw_anim = (switch_anim_t*)sw_btn->private_data;
            anim_pos = sgl_widget_draw_ofs_x(sw_anim->pos);
        }
        else {
            anim_pos = sgl_widget_draw_ofs_x(rect.x2 - toggle_w);
        }
        if(obj_img == NULL) {
            dot_color = sgl_color_mixer(obj->style->body_color, SGL_WHITE, 128);
            if(bg_img == NULL) {
                sgl_draw_round_rect_solid(surf, rect, rect_radius, obj->style->body_color, obj->parent->style->body_color);
            }
            else {
                sgl_draw_round_rect_solid_on_bg(surf, rect, rect_radius, obj->style->body_color);
            }
            if(anim_pos >= (rect.x2 - toggle_w - 1)) {
                rect.x1 = rect.x2 - toggle_w - SWITCH_BORDER;
                rect.x2 = rect.x2 - SWITCH_BORDER; 
                rect.y1 += SWITCH_BORDER;
                rect.y2 -= SWITCH_BORDER;
            }
            else {
                rect.x1 = anim_pos + SWITCH_BORDER; 
                rect.x2 = rect.x1 + toggle_w;
                rect.y1 += SWITCH_BORDER;
                rect.y2 -= SWITCH_BORDER;
            }
            sgl_draw_round_rect_solid(surf, rect, rect_radius - SWITCH_BORDER, dot_color, obj->style->body_color);
        }
        else {
            if(bg_img == NULL) {
                sgl_draw_round_rect_img(surf, rect, rect_radius, obj->parent->style->body_color, obj_img);
            }
            else {
                sgl_draw_round_rect_img_on_bg(surf, rect, rect_radius, obj_img);
            }
        }
    }
    else {
        if(sw_btn->private_data != NULL) {
            sw_anim = (switch_anim_t*)sw_btn->private_data;
            anim_pos = sgl_widget_draw_ofs_x(sw_anim->pos + SWITCH_BORDER);
        }
        else {
            anim_pos = sgl_widget_draw_ofs_x(0);
        }
        if(sw_btn->toggle_img == NULL) {
            dot_color = sgl_color_mixer(sw_btn->toggle_color, SGL_WHITE, 128);
            if(bg_img == NULL) {
                sgl_draw_round_rect_solid(surf, rect, rect_radius, sw_btn->toggle_color, obj->parent->style->body_color);
            }
            else {
                sgl_draw_round_rect_solid_on_bg(surf, rect, rect_radius, sw_btn->toggle_color);
            }
            if(anim_pos <= 0) {
                rect.x1 += SWITCH_BORDER;
                rect.x2 = rect.x1 + toggle_w; 
                rect.y1 += SWITCH_BORDER;
                rect.y2 -= SWITCH_BORDER;
            }
            else {
                rect.x1 = anim_pos + SWITCH_BORDER; 
                rect.x2 = rect.x1 + toggle_w;
                rect.y1 += SWITCH_BORDER;
                rect.y2 -= SWITCH_BORDER;
            }
            sgl_draw_round_rect_solid(surf, rect, rect_radius - SWITCH_BORDER, dot_color, sw_btn->toggle_color);
        }
        else {
            if(bg_img == NULL) {
                sgl_draw_round_rect_img(surf, rect, rect_radius, obj->parent->style->body_color, sw_btn->toggle_img);
            }
            else {
                sgl_draw_round_rect_img_on_bg(surf, rect, rect_radius, sw_btn->toggle_img);
            }
        }
    }

    sgl_draw_obj_selected(obj, surf, rect, obj->style->body_color);
    sgl_widget_draw_buffer_flush(obj, surf);
}


void sgl_switch_set_toggle_color(sgl_obj_t *obj, sgl_color_t color)
{
    sgl_switch_t *sw = container_of(obj, sgl_switch_t, obj);
    sw->toggle_color = color;
}

void sgl_switch_set_toggle_img(sgl_obj_t *obj, sgl_img_t *img)
{
    sgl_switch_t *sw = container_of(obj, sgl_switch_t, obj);
    sw->toggle_img = img;
}

void sgl_switch_set_status(sgl_obj_t *obj, bool status)
{
    sgl_switch_t *sw_btn = container_of(obj, sgl_switch_t, obj);
    sw_btn->status = status;
}


bool sgl_switch_get_status(sgl_obj_t* obj)
{
    sgl_switch_t *sw_btn = container_of(obj, sgl_switch_t, obj);
    return sw_btn->status;
}


int sgl_switch_set_anim(sgl_obj_t *obj, bool flag)
{
    sgl_switch_t *sw = container_of(obj, sgl_switch_t, obj);
    switch_anim_t *sw_anim = (switch_anim_t *)sgl_malloc(sizeof(switch_anim_t));
    if(sw_anim == NULL) {
        return -1;
    }
    sw_anim->pos = SWITCH_BORDER;
    sgl_anim_t *anim = &sw_anim->anim;
    sw->private_data = sw_anim;
    sgl_anim_bind_obj(anim, obj);
    sgl_anim_set_interval(anim, 10);
    sgl_anim_active(anim);
    sgl_anim_set_endless(anim, true);
    sgl_anim_set_anim_cb(anim, switch_anim_cb, NULL);
    sgl_anim_exec(anim);
    return 0;
}


static void sgl_switch_event_cb(sgl_obj_t *obj)
{
    switch_anim_t *sw_anim;
    if(obj->ev_stat == SGL_EVENT_PRESSED) {
        sgl_switch_t *sw_btn = container_of(obj, sgl_switch_t, obj);
        sw_btn->status = !sw_btn->status;
        if(sw_btn->private_data != NULL) {
            sw_anim = (switch_anim_t *)sw_btn->private_data;
            sgl_anim_active(&sw_anim->anim);
        }
        sgl_obj_event_cb(obj);
        sgl_switch_draw(obj);
    }
    else if(obj->ev_stat == SGL_EVENT_FORCE_DRAW) {
        sgl_switch_draw(obj);
    }
}
