/* source/widget/sgl_mesgbox.h
 *
 * MIT License
 *
 * Copyright(c) 2023-present All contributors of SGL  
 *  
 * Document reference link: www.sgl-io.cn
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


#ifndef __SGL_MESSAGEBOX_H__
#define __SGL_MESSAGEBOX_H__

#include "../core/sgl_core.h"

#ifdef __cplusplus
extern "C" {
#endif


#define SGL_MESSAGE_TYPE_YES_NO             1
#define SGL_MESSAGE_TYPE_OK                 2
#define SGL_MESSAGE_TYPE_ERROR              3
#define SGL_MESSAGE_TYPE_WARNING            4
#define SGL_MESSAGE_TYPE_INFO               5



typedef struct sgl_mesgbox {
    sgl_obj_t obj;
    const char *title;
    const char *text;
    sgl_font_t *font;
    uint8_t  mesgbox_type;

}sgl_mesgbox_t;



sgl_obj_t* sgl_mesgbox_create(sgl_obj_t* parent, uint8_t type);
void sgl_mesgbox_set_font(sgl_obj_t *obj, sgl_font_t *font);
void sgl_mesgbox_set_text(sgl_obj_t *obj, const char *text);
int sgl_mesgbox_exit(sgl_obj_t *obj);



#ifdef __cplusplus
}
#endif

#endif //__SGL_MESSAGEBOX_H__