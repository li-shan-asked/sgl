/* source/widget/sgl_listview.h
 *
 * MIT License
 *
 * Copyright(c) 2023-present All contributors of SGL  
 *  
 * Document reference link: www.sgl-io.cn
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


#ifndef __SGL_LISTVIEW_H__
#define __SGL_LISTVIEW_H__

#include "../core/sgl_core.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct sgl_listview_item {
    sgl_list_node_t node;
    const char *text;
    uint8_t item_type;
    void (*event_fn)(struct sgl_obj *obj, void *data);
    void *data;
    const sgl_icon_t *icon;

}sgl_listview_item_t;


typedef struct sgl_listview {
    sgl_obj_t obj;
    sgl_font_t *font;
    sgl_list_node_t head;
    int  cur_index;
    int count;

}sgl_listview_t;



sgl_obj_t* sgl_listview_create(sgl_obj_t* parent);
void sgl_listview_set_font(sgl_obj_t *obj, sgl_font_t *font);
int sgl_listview_add_item(sgl_obj_t *obj,
                          const char *text, uint8_t type, const sgl_icon_t *icon,
                          void(*event_fn)(struct sgl_obj *obj, void *data), 
                          void *data);

int sgl_listview_del_item(sgl_obj_t *obj, int index);
int sgl_listview_del_last_item(sgl_obj_t *obj);



#ifdef __cplusplus
}
#endif

#endif //__SGL_LISTVIEW_H__
