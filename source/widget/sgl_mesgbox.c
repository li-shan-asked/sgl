/* source/widget/sgl_mesgbox.c
 *
 * MIT License
 *
 * Copyright(c) 2023-present All contributors of SGL  
 *  
 * Document reference link: www.sgl-io.cn
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "./sgl_mesgbox.h"
#include "../core/sgl_obj.h"
#include "../core/sgl_page.h"
#include "../core/sgl_device.h"
#include "../core/sgl_task.h"
#include "../stdlib/sgl_mem.h"
#include "../stdlib/sgl_string.h"
#include "../draw/sgl_draw.h"
#include "../draw/sgl_draw_rect.h"
#include "../draw/sgl_draw_font.h"
#include "../draw/sgl_draw_circle.h"
#include "../draw/sgl_draw_icon.h"
#include "../draw/sgl_draw_arc.h"


static void sgl_mesgbox_event_cb(sgl_obj_t *obj);


int sgl_mesgbox_init(sgl_mesgbox_t *mesgbox, sgl_obj_t* parent)
{
    sgl_obj_t* obj;
    if(parent == NULL) {
        return -1;
    }
    obj = &mesgbox->obj;
    obj->event_data = NULL;
    obj->ev_stat = SGL_EVENT_FORCE_DRAW;
    obj->parent = parent;
    obj->dirty = 1;
    obj->selected = 0;
    obj->hide = 0;
    obj->draw_cb = sgl_mesgbox_event_cb;
    sgl_page_add_obj(parent, obj);
    return 0;
}

sgl_obj_t* sgl_mesgbox_create(sgl_obj_t* parent, uint8_t type)
{
    int ret;
    sgl_mesgbox_t *mesgbox;
    if(parent == NULL) {
        return NULL;
    }
    mesgbox = (sgl_mesgbox_t *)sgl_malloc(sizeof(sgl_mesgbox_t));
    if(mesgbox == NULL)
        return NULL;
    
    ret = sgl_mesgbox_init(mesgbox, parent);
    mesgbox->mesgbox_type = type;
    if(ret) {
        return NULL;
    }
    return  &mesgbox->obj;
}


static void sgl_mesgbox_draw(sgl_obj_t* obj)
{
    sgl_surf_t *surf = sgl_get_active_surf(obj);
    sgl_img_t *bg_img = sgl_obj_get_bg_img(obj->parent);
    sgl_mesgbox_t *mesgbox = container_of(obj, sgl_mesgbox_t, obj);
    sgl_widget_draw_rect(rect);
    sgl_widget_buffer_valid(obj);

    //if font is NULL, do nothing, because of no font can be used
    if(mesgbox->font == NULL) {
        return;
    }
    if(bg_img == NULL) {
        sgl_draw_obj_buffer_clear(surf, obj, obj->parent->style->body_color);

        if(obj->ev_stat == SGL_EVENT_FORCE_DRAW_CLEAR) {

            sgl_widget_draw_buffer_flush(obj, surf);
            sgl_obj_collide_redraw_all(obj);
            return;
        }

        // sgl_draw_round_rect_solid_thalf(surf,
        //                 rect,
        //                 obj->style->radius,
        //                 obj->style->body_color,
        //                 obj->parent->style->body_color
        //                 );
        
        //sgl_draw_circle_solid_flush(surf, rect.x1 + obj->size.w / 2, rect.y1 + obj->size.h / 2, obj->style->radius, obj->style->body_color, obj->parent->style->body_color);
       // sgl_draw_circle_solid_lt_flush(surf, obj->pos.x + obj->size.w / 2, obj->pos.y + obj->size.h / 2, obj->style->radius, obj->style->body_color, obj->parent->style->body_color);

        //sgl_widget_draw_buffer_flush(obj, surf);
    }
    else {
        sgl_draw_obj_buffer_pick_img(surf, obj, bg_img);

        if(obj->ev_stat == SGL_EVENT_FORCE_DRAW_CLEAR) {

            sgl_widget_draw_buffer_flush(obj, surf);
            sgl_obj_collide_redraw_all(obj);
            return;
        }

        // sgl_draw_round_rect_solid_thalf_on_bg(surf,
        //                 rect,
        //                 obj->style->radius,
        //                 obj->style->body_color
        //                 );
        //sgl_draw_circle_solid_lt_flush(surf, obj->pos.x + obj->size.w / 2, obj->pos.y + obj->size.h / 2, obj->style->radius, obj->style->body_color, obj->parent->style->body_color);

        //sgl_widget_draw_buffer_flush(obj, surf);
    }
}


static void sgl_mesgbox_event_cb(sgl_obj_t *obj)
{
    if(obj->ev_stat == SGL_EVENT_FORCE_DRAW || obj->ev_stat == SGL_EVENT_FORCE_DRAW_CLEAR) {
        sgl_mesgbox_draw(obj);
    }
}


void sgl_mesgbox_set_font(sgl_obj_t *obj, sgl_font_t *font)
{
    sgl_mesgbox_t *mesgbox = container_of(obj, sgl_mesgbox_t, obj);
    mesgbox->font = font;
}

void sgl_mesgbox_set_text(sgl_obj_t *obj, const char *text)
{
    sgl_mesgbox_t *mesgbox = container_of(obj, sgl_mesgbox_t, obj);
    mesgbox->text = text;
}


int sgl_mesgbox_exit(sgl_obj_t *obj)
{
    obj->dirty = 1;
    obj->ev_stat = SGL_EVENT_FORCE_DRAW_CLEAR;
    return 0;
}

