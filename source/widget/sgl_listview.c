/* source/widget/sgl_listview.c
 *
 * MIT License
 *
 * Copyright(c) 2023-present All contributors of SGL  
 *  
 * Document reference link: www.sgl-io.cn
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


#include "./sgl_listview.h"
#include "../core/sgl_obj.h"
#include "../core/sgl_page.h"
#include "../core/sgl_device.h"
#include "../core/sgl_task.h"
#include "../stdlib/sgl_mem.h"
#include "../stdlib/sgl_string.h"
#include "../draw/sgl_draw.h"
#include "../draw/sgl_draw_rect.h"
#include "../draw/sgl_draw_font.h"
#include "../draw/sgl_draw_circle.h"
#include "../draw/sgl_draw_icon.h"


static void sgl_listview_event_cb(sgl_obj_t *obj);

int sgl_listview_init(sgl_listview_t *listview, sgl_obj_t* parent)
{
    sgl_obj_t* obj;
    if(parent == NULL) {
        return -1;
    }
    obj = &listview->obj;
    obj->event_data = NULL;
    obj->ev_stat = SGL_EVENT_FORCE_DRAW;
    obj->parent = parent;
    obj->dirty = 1;
    obj->selected = 0;
    obj->hide = 0;
    obj->draw_cb = sgl_listview_event_cb;
    sgl_page_add_obj(parent, obj);
    return 0;
}


sgl_obj_t* sgl_listview_create(sgl_obj_t* parent)
{
    int ret;
    sgl_listview_t *listview;
    if(parent == NULL) {
        return NULL;
    }
    listview = (sgl_listview_t *)sgl_malloc(sizeof(sgl_listview_t));
    if(listview == NULL)
        return NULL;
    
    ret = sgl_listview_init(listview, parent);
    if(ret) {
        return NULL;
    }
    //init list head
    sgl_list_init(&listview->head);
    listview->count = 0;
    listview->cur_index = 0;

    return  &listview->obj;
}


int sgl_listview_add_item(sgl_obj_t *obj,
                          const char *text, uint8_t type, const sgl_icon_t *icon,
                          void(*event_fn)(struct sgl_obj *obj, void *data), 
                          void *data)
{
    sgl_listview_t *listview = container_of(obj, sgl_listview_t, obj);
    sgl_listview_item_t *item = (sgl_listview_item_t *)sgl_malloc(sizeof(sgl_listview_item_t));
    if(item == NULL) {
        return -1;
    }
    item->text = text;
    item->item_type = type;
    item->event_fn = event_fn;
    item->data = data;
    item->icon = icon;
    
    sgl_list_add_node_at_tail(&listview->head, &item->node);
    listview->count ++;

    return 0;
}

int sgl_listview_del_item(sgl_obj_t *obj, int index)
{
    sgl_listview_t *listview = container_of(obj, sgl_listview_t, obj);
    if(index > listview->count) {
        return -1;
    }

    sgl_list_node_t *pos_node = &listview->head;
    while(index -- ){
        pos_node = sgl_list_next_node(pos_node);
    }

    sgl_list_del_node(pos_node);
    listview->count --;

    return 0;
}

int sgl_listview_del_last_item(sgl_obj_t *obj)
{
    sgl_listview_t *listview = container_of(obj, sgl_listview_t, obj);
    if(listview->count <= 0) {
        return -1;
    }
    sgl_list_del_tail_node(&listview->head);
    listview->count --;

    return 0;
}


void sgl_listview_set_font(sgl_obj_t *obj, sgl_font_t *font)
{
    sgl_listview_t *listview = container_of(obj, sgl_listview_t, obj);
    listview->font = font;
}


#define SGL_CONFIG_FRAMEBUFFER_MMAP 1
void sgl_listview_draw(sgl_obj_t *obj)
{
    sgl_surf_t *surf = sgl_get_active_surf(obj);
    sgl_listview_t *listview = container_of(obj, sgl_listview_t, obj);
#if SGL_CONFIG_FRAMEBUFFER_MMAP
    int item_height = obj->pos.y;
    sgl_pos_t icon_pos;
    sgl_pos_t text_pos;
    int filled_item = 0;

    sgl_list_node_t *current_node = &listview->head;
    sgl_listview_item_t *current_item;

    sgl_obj_t tmp_obj = {.size = obj->size};
    
    sgl_rect_t rect = {.x1 = obj->pos.x, .y1 = obj->pos.y, .x2 = (int16_t)(obj->size.w - 1), .y2 = (int16_t)(obj->pos.y)};

    for(int i = 0; i < listview->count; i ++) {

        current_node = sgl_list_next_node(current_node);
        current_item = container_of(current_node, sgl_listview_item_t, node);

        item_height = sgl_max(sgl_text_height(listview->font), current_item->icon->height) + 8;
        rect.y2 = rect.y1 + item_height - 2;

        icon_pos.y = (item_height - current_item->icon->width) / 2 + rect.y1;
        text_pos.y = (item_height - sgl_text_height(listview->font)) / 2 + rect.y1;

        tmp_obj.pos.x = rect.x1;
        tmp_obj.pos.y = rect.y1;
        tmp_obj.size.h = item_height;


        if(obj->parent->bg_img == NULL) {
            sgl_draw_round_rect_solid(surf, rect, obj->style->radius, obj->style->body_color, obj->parent->style->body_color);
        }
        else {
            sgl_draw_obj_buffer_pick_img(surf, &tmp_obj, obj->parent->bg_img);
        }

        if(obj->alpha == 0) {
            if(obj->parent->bg_img == NULL) {
                sgl_draw_round_rect_solid(surf, rect, obj->style->radius, 
                                            obj->style->body_color, obj->parent->style->body_color);

                sgl_draw_icon(surf, obj->pos.x + obj->style->radius, icon_pos.y, obj->style->text_color, 
                                obj->style->body_color, current_item->icon);

                sgl_draw_font_string(surf, obj->pos.x + current_item->icon->width + obj->style->radius + 4, text_pos.y, 
                                    current_item->text, obj->style->text_color, 
                                    obj->style->body_color, listview->font);
            }
            else {

                sgl_draw_round_rect_solid_on_bg(surf, rect, obj->style->radius, 
                                                 obj->style->body_color);

                sgl_draw_icon_on_bg(surf, obj->pos.x + obj->style->radius, icon_pos.y, obj->style->text_color, 
                                      current_item->icon);

                sgl_draw_font_string_on_bg(surf, obj->pos.x + current_item->icon->width + obj->style->radius + 4, text_pos.y, 
                                    current_item->text, obj->style->text_color, 
                                    listview->font);
            }

        }
        else {
            if(obj->parent->bg_img == NULL) {
                sgl_draw_round_rect_transp_on_bg(surf, rect, obj->style->radius, 
                                                  obj->style->body_color, obj->alpha);

                sgl_draw_icon_transp(surf, obj->pos.x + obj->style->radius, icon_pos.y, obj->style->text_color, 
                                obj->style->body_color, current_item->icon, obj->alpha);

                //TODO:
                sgl_draw_font_string_on_bg(surf, obj->pos.x + current_item->icon->width + obj->style->radius + 4, text_pos.y, 
                                    current_item->text, obj->style->text_color, 
                                    listview->font);
            }
            else {
                sgl_draw_round_rect_transp_on_bg(surf, rect, obj->style->radius, 
                                            obj->style->body_color, obj->alpha);

                sgl_draw_icon_transp_on_bg(surf, obj->pos.x + obj->style->radius, icon_pos.y, obj->style->text_color, 
                                      current_item->icon, obj->alpha);

                sgl_draw_font_string_transp_on_bg(surf, obj->pos.x + current_item->icon->width + obj->style->radius + 4, text_pos.y, 
                                    current_item->text, obj->style->text_color, 
                                    listview->font, obj->alpha);
            }
        }

        filled_item += item_height;
        rect.y1 += item_height;
    }
    while(rect.y2 < (obj->pos.y + obj->size.h)) {
        rect.y2 += item_height;

        if(obj->alpha == 0) {
            if(obj->parent->bg_img == NULL) {
                sgl_draw_round_rect_solid(surf, rect, obj->style->radius, 
                                            obj->style->body_color, obj->parent->style->body_color); 
            }
            else {
                sgl_draw_round_rect_solid_on_bg(surf, rect, obj->style->radius, 
                                obj->style->body_color);
            }
        }
        else {
            if(obj->parent->bg_img == NULL) {
                //TODO:
                sgl_draw_round_rect_transp_on_bg(surf, rect, obj->style->radius, 
                                                  obj->style->body_color, obj->alpha);
            }
            else {
                sgl_draw_round_rect_transp_on_bg(surf, rect, obj->style->radius, 
                                            obj->style->body_color, obj->alpha);
            }
        }
        filled_item += item_height;
        rect.y1 += item_height;
    }



#else


#endif
}


static void sgl_listview_event_cb(sgl_obj_t *obj)
{
    if(obj->ev_stat == SGL_EVENT_PRESSED) {
        //call callback function of index item


        sgl_listview_draw(obj);
    }
    else if(obj->ev_stat == SGL_EVENT_FORCE_DRAW) {
        sgl_listview_draw(obj);
    }
}