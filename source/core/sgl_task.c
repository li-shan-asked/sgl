/* source/core/sgl_task.c
 *
 * MIT License
 *
 * Copyright(c) 2023-present All contributors of SGL  
 *  
 * Document reference link: www.sgl-io.cn
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "sgl_conf.h"
#include "./sgl_task.h"
#include "./sgl_event.h"
#include "../stdlib/sgl_mem.h"
#include "../stdlib/sgl_string.h"

SGL_LIST_HEAD(sgl_task_list_head);


/**
 * @brief create a task
 * 
 * @param task_fun: pointer to task function
 * @param data: task paramater data
 * 
 * @return sgl_task_t*: task handler
*/
sgl_task_t* sgl_task_create(int (*task_fun)(void *data), void *data)
{
    sgl_task_t* task = (sgl_task_t*)sgl_malloc(sizeof(sgl_task_t));
    if(task != NULL) {
        // init task func and data
        task->task_fun = task_fun;
        task->data = data;
        // init task status，disable
        task->status = false;
        // add task to task list
        sgl_list_add_node_at_tail(&sgl_task_list_head, &task->node);
    }
    return task;
}


/**
 * @brief delete a task
 * 
 * @param task: task handler
 * 
 * @return none
*/
void sgl_task_delete(sgl_task_t* task)
{
    sgl_list_del_node(&task->node);
    sgl_free(task);
}


/**
 * @brief clear all tasks
 * 
 * @param none
 * 
 * @return none
*/
void sgl_task_clear(void)
{
    sgl_list_init(&sgl_task_list_head);
}


/**
 * @brief check if task list is empty
 * 
 * @param none
 * 
 * @return none
*/
bool sgl_task_is_empty(void)
{
    return sgl_list_is_empty(&sgl_task_list_head);
}


/**
 * @brief run a task
 * 
 * @param task: task handler
 * 
 * @return none
*/
void sgl_task_run(sgl_task_t* task)
{
    task->status = true;
}


/**
 * @brief stop a task
 * 
 * @param task: task handler
 * 
 * @return none
*/
void sgl_task_stop(sgl_task_t* task)
{
    task->status = false;
}


/**
 * @brief run all tasks
 * 
 * @param none
 * 
 * @return none
 * 
*/
void sgl_task_run_all(void)
{
    sgl_list_node_t *head_node = &sgl_task_list_head;
    sgl_list_node_t *current_node;
    sgl_task_t *current_task;

    sgl_list_for_each(current_node, head_node) {
        //get current task handler
        current_task = container_of(current_node, sgl_task_t, node);
        if(current_task->status) {
            // run task
            current_task->task_fun(current_task->data);
        }
    }
}
