/* source/core/sgl_obj.c
 *
 * MIT License
 *
 * Copyright(c) 2023-present All contributors of SGL  
 *  
 * Document reference link: www.sgl-io.cn
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "./sgl_obj.h"
#include "./sgl_page.h"
#include "./sgl_device.h"


/**
* @brief Obtain the alignment position of the object, note that the position 
*        here is calculated based on the relative position
*
* @param parent_size       size of parent object
* @param size              size of object
* @param size    The parent of the object
*
* @return sgl_pos_t  The calculated position
*/
sgl_pos_t sgl_get_align_pos(sgl_size_t *parent_size, sgl_size_t *size, sgl_align_type_e type)
{
    sgl_pos_t ret = {.x = 0, .y = 0};
    switch(type) {                
        case SGL_ALIGN_TOP_MID:          
            ret.x = (parent_size->w - size->w)/2;
            ret.y = 0;
        break;

        case SGL_ALIGN_TOP_LEFT:
            ret.x = 0;
            ret.y = 0;
        break; 
            
        case SGL_ALIGN_TOP_RIGHT:    
            ret.x = parent_size->w - size->w;
            ret.y = 0;
        break;

        case SGL_ALIGN_CENTER_MID:
            ret.x = (parent_size->w - size->w)/2;
            ret.y = (parent_size->h - size->h)/2;
        break;
        
        case SGL_ALIGN_CENTER_LEFT:
            ret.x = 0;
            ret.y = (parent_size->h - size->h)/2;
        break;

        case SGL_ALIGN_CENTER_RIGHT:
            ret.x = parent_size->w - size->w ;
            ret.y = (parent_size->h - size->h)/2;
        break;

        case SGL_ALIGN_BOT_LEFT:
            ret.x = 0;
            ret.y = parent_size->h - size->h;
        break;

        case SGL_ALIGN_BOT_MID:
            ret.x = (parent_size->w - size->w)/2;
            ret.y = parent_size->h - size->h;
        break;

        case SGL_ALIGN_BOT_RIGHT:
            ret.x = parent_size->w - size->w;
            ret.y = parent_size->h - size->h;
        break;

        case SGL_ALIGN_TOP: 
            ret.y = 0;
        break;
        case SGL_ALIGN_CENTER: 
            ret.y = (parent_size->h - size->h)/2;
        break;
        case SGL_ALIGN_BOT: 
            ret.y = parent_size->h - size->h;
        break;
        case SGL_ALIGN_LEFT: 
            ret.x = 0;
        break;
        case SGL_ALIGN_MID: 
            ret.x = (parent_size->w - size->w)/2;
        break;
        case SGL_ALIGN_RIGHT: 
            ret.x = parent_size->w - size->w; 
        break;
        default: break;
    }
    return ret;
}


/**
 * @brief Check whether the two objects are collided
 * 
 * @param obj1:  object 1
 * @param obj2:  object 2
 * 
 * @return bool: true is collided, false is not collided
*/
bool sgl_obj_is_collide_with_obj(sgl_obj_t *obj1, sgl_obj_t *obj2)
{
    // check position x
    if (obj1->pos.x > obj2->pos.x + obj2->size.w ||
        obj2->pos.x > obj1->pos.x + obj1->size.w) {
        return false;
    }
    // check position y
    if (obj1->pos.y > obj2->pos.y + obj2->size.h ||
        obj2->pos.y > obj1->pos.y + obj1->size.h) {
        return false;
    }
    return true;
}


/**
 * @brief Check whether the object are collided with rect area
 * 
 * @param obj:  object
 * @param rect: rect area
 * 
 * @return bool: true is collided, false is not collided
*/
bool sgl_obj_is_collide_with_rect(sgl_obj_t *obj, sgl_rect_t *rect)
{
    // check position x
    if (obj->pos.x > rect->x2 ||
        rect->x1 > obj->pos.x + obj->size.w) {
        return false;
    }
    // check position y
    if (obj->pos.y > rect->y2 ||
        rect->y1 > obj->pos.y + obj->size.h) {
        return false;
    }
    return true;
}


/**
 * @brief Check whether rect1 area are collided with rect2 area
 * 
 * @param rect1: rect1 area
 * @param rect2: rect2 area
 * 
 * @return bool: true is collided, false is not collided
*/
bool sgl_rect_is_collide_with_rect(sgl_rect_t *rect1, sgl_rect_t *rect2)
{
    // check position x
    if (rect1->x1 > rect2->x2 ||
        rect2->x1 > rect1->x2) {
        return false;
    }
    // check position y
    if (rect1->y1 > rect2->y2 ||
        rect2->y1 > rect1->y2) {
        return false;
    }
    return true;
}


/**
 * @brief Check for objects that collide. If they do, all controls will be redrawn. Otherwise, nothing will be done.
 * 
 * @param obj: widget object
 * 
 * @return none
*/
void sgl_obj_collide_redraw_all(sgl_obj_t *obj)
{
    sgl_obj_t *page = sgl_page_get_active();
    sgl_obj_t *current_obj;
    sgl_list_node_t *current_node;
    sgl_list_node_t *node_head = sgl_page_get_obj_head(page);

    //page is not NULL, do nothing
    if(node_head == NULL) {
        return;
    }
    //foreach all object, check the object is whether collide with obj
    sgl_list_for_each(current_node, node_head) {
        current_obj = container_of(current_node, struct sgl_obj, node);
        
        if(obj != current_obj) {
            //if the object is collided with obj, set dirty as true,
            //then force redraw the object
            if(sgl_obj_is_collide_with_obj(obj, current_obj)) {
                current_obj->dirty = 1;
                sgl_obj_set_event_status(current_obj, SGL_EVENT_FORCE_DRAW);
            }
        }
    }
}
