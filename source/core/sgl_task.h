/* source/core/sgl_task.h
 *
 * MIT License
 *
 * Copyright(c) 2023-present All contributors of SGL  
 *  
 * Document reference link: www.sgl-io.cn
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef __SGL_TASK_H__
#define __SGL_TASK_H__

#include "./sgl_core.h"

#ifdef __cplusplus
extern "C" {
#endif


/**
 * @brief create a task
 * 
 * @param task_fun: pointer to task function
 * @param data: task paramater data
 * 
 * @return sgl_task_t*: task handler
*/
sgl_task_t* sgl_task_create(int (*task_fun)(void *data), void *data);


/**
 * @brief delete a task
 * 
 * @param task: task handler
 * 
 * @return none
*/
void sgl_task_delete(sgl_task_t* task);


/**
 * @brief clear all tasks
 * 
 * @param none
 * 
 * @return none
*/
void sgl_task_clear(void);


/**
 * @brief check if task list is empty
 * 
 * @param none
 * 
 * @return none
*/
bool sgl_task_is_empty(void);


/**
 * @brief run a task
 * 
 * @param task: task handler
 * 
 * @return none
*/
void sgl_task_run(sgl_task_t* task);


/**
 * @brief create and run it
 * 
 * @param task_fun: pointer to task function
 * @param data: task paramater data
 * 
 * @return sgl_task_t*: task handler
*/
static inline sgl_task_t* sgl_task_create_run(int (*task_fun)(void *data), void *data)
{
    sgl_task_t *task = sgl_task_create(task_fun, data);
    sgl_task_run(task);
    return task;
}


/**
 * @brief stop a task
 * 
 * @param task: task handler
 * 
 * @return none
*/
void sgl_task_stop(sgl_task_t* task);


/**
 * @brief run all tasks
 * 
 * @param none
 * 
 * @return none
 * 
*/
void sgl_task_run_all(void);


#ifdef __cplusplus
} /*extern "C"*/
#endif

#endif //__SGL_TASK_H__
