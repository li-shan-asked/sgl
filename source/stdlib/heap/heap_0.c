#include "./heap_0.h"
#include "sgl_conf.h"

#if SGL_CONFIG_HEAP_POLICY == 0

static void* mem_pool = NULL;
static size_t all_mem_size = 0;
static size_t used_mem_size = 0;

#define SGL_CONFIG_HEAP_ALIGN  4
#define SGL_HEAP_ALIGN_MASK (SGL_CONFIG_HEAP_ALIGN - 1)


void sgl_heap0_init(void *mem, size_t size)
{
    mem_pool = mem;
    all_mem_size = size;
}


/**
 * @brief  sgl memory alloc, the function is unsafe, you should ensure that 
 *         the requested size is smaller than the free size of memory
 * 
 * @param  size   request size of memory
 * 
 * @return point to request memory address
*/
void* sgl_heap0_malloc(size_t size)
{
    void *ret = NULL;
    if((size & SGL_HEAP_ALIGN_MASK) != 0) {
        size += (SGL_CONFIG_HEAP_ALIGN - (size & SGL_HEAP_ALIGN_MASK));
    }
    if(size <= (all_mem_size - used_mem_size)) {
        ret = (void*)&((char*)mem_pool)[used_mem_size];
        used_mem_size += size;
    }
    return ret;
}


/**
 * @brief  sgl memory free
 * 
 * @param  p  the pointer of request size of memory
 * 
 * @return none
*/
void sgl_heap0_free(void* p)
{
    //nothing to do
}


/**
 * @brief  get memory information, include total memory size, free size and used size
 * 
 * @param  none
 * 
 * @return memory information
*/
sgl_mem_monitor_t sgl_heap0_mem_get_monitor(void)
{
    sgl_mem_monitor_t info = { .total_size = all_mem_size,
                               .free_size = all_mem_size - used_mem_size,
                               .used_size = used_mem_size,
                               .used_rate = (float)(used_mem_size / all_mem_size),
                             };
    return info;
}

#endif //SGL_CONFIG_HEAP_POLICY == 0
