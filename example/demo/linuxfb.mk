all:
	@make -j4 -s -C ../platform/linuxfb -f Makefile all

clean:
	@make -j4 -s -C ../platform/linuxfb -f Makefile clean
