all:
	@make -j4 -s -C ../platform/sdl2 -f Makefile all

clean:
	@make -j4 -s -C ../platform/sdl2 -f Makefile clean
