#include "../../../source/sgl.h"
#include "../../common/background.h"
#include "../../common/icon.h"

static sgl_img_t bg_img = {.bitmap = bg_test, .width = 240, .height = 320,};
sgl_obj_t *second_page;


static sgl_obj_t *msg;
static void btn_event(sgl_obj_t *obj, void *data)
{
    if(sgl_ev_stat(obj) == SGL_EVENT_PRESSED) {
        SGL_LOG_INFO("buttton pressed");
        //sgl_page_set_active(second_page);
        //sgl_mesgbox_exit(msg);
    }
    else if(sgl_ev_stat(obj) == SGL_EVENT_RELEASED) {
        SGL_LOG_INFO("buttton released");
        
    };
}


sgl_style_t btn_style = {.body_color = SGL_BLUE, .text_color = SGL_RED, .radius = 50};
sgl_obj_t* btn;
sgl_obj_t *main_page;

static sgl_img_t app_icon = {.bitmap = icon_test, .width = 50, .height = 50,};
void demo_setup(void)
{
    main_page = sgl_page_create();
    second_page=sgl_page_create();

    sgl_obj_set_bg_img(main_page, &bg_img);

    sgl_page_set_active(main_page);

    SGL_LOG_INFO("arcbar demo");

    //定义一个样式，主体颜色为SGL_BLUE，文本颜色为SGL_RED，圆角半径为16
    static sgl_style_t arcbar_style = {.body_color = SGL_BLUE, .text_color = SGL_RED};
    //创建一个弧形指示器对象
    static sgl_obj_t *arcbar = sgl_arcbar_create(main_page, 68);
    //设置弧形指示器的大小为宽100和高100
    sgl_obj_set_size(arcbar, 100, 100);
    //设置弧形指示器的样式为arcbar_style
    sgl_obj_set_style(arcbar, &arcbar_style);
    //设置弧形指示器的位置为x坐标170, y坐标120
    sgl_obj_set_pos(arcbar, 80, 120);
    //设置弧形指示器的字体
    sgl_arcbar_set_font(arcbar, &cascadia_mono17);
    //设置弧形指示器的厚度为10
    sgl_arcbar_set_thick(arcbar, 10);

}
