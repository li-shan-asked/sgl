#include "../../../source/sgl.h"
#include "../../common/background.h"
#include "../../common/icon.h"

static sgl_img_t bg_img = {.bitmap = bg_test, .width = 240, .height = 320,};
sgl_obj_t *second_page;


static sgl_obj_t *msg;
static void btn_event(sgl_obj_t *obj, void *data)
{
    if(sgl_ev_stat(obj) == SGL_EVENT_PRESSED) {
        SGL_LOG_INFO("buttton pressed");
        //sgl_page_set_active(second_page);
        //sgl_mesgbox_exit(msg);
    }
    else if(sgl_ev_stat(obj) == SGL_EVENT_RELEASED) {
        SGL_LOG_INFO("buttton released");
        
    };
}


sgl_style_t btn_style = {.body_color = SGL_BLUE, .text_color = SGL_RED, .radius = 50};
sgl_obj_t* btn;
sgl_obj_t *main_page;

static sgl_img_t app_icon = {.bitmap = icon_test, .width = 50, .height = 50,};
void demo_setup(void)
{
    main_page = sgl_page_create();
    second_page=sgl_page_create();

    sgl_obj_set_bg_img(main_page, &bg_img);

    sgl_page_set_active(main_page);

    SGL_LOG_INFO("button demo");
    //定义一个样式，主体颜色为SGL_BLUE，文本颜色为SGL_RED，圆角半径为16
    //创建一个按钮对象
    btn = sgl_button_create(main_page);
    //设置按钮的尺寸为宽98和高32
    sgl_obj_set_size(btn, 98, 32);
    //设置按钮的位置为x坐标100, y坐标50
    sgl_obj_set_pos(btn, 100, 50);
    //设置按钮的样式为btn_style
    sgl_obj_set_style(btn, &btn_style);
    //设置按钮的事件回调函数为btn_event，参数无
    sgl_obj_set_event_cb(btn, btn_event, NULL);
    //设置按钮的按下后的颜色为SGL_GREEN
    sgl_button_set_toggle_color(btn, SGL_GREEN);
    //设置按钮的字体为song12
    sgl_button_set_font(btn, &song12);
    // //设置按钮的显示文本为Button
    sgl_button_set_text(btn, "Button");

    static const sgl_style_t app_style = {.body_color = SGL_BLUE, .text_color = SGL_RED, .radius = 15};
    sgl_obj_t *app = sgl_appstart_create(main_page);
    sgl_obj_set_style(app, &app_style);
    sgl_obj_set_size(app, 45, 45);
    sgl_obj_set_pos(app, 20, 160);
    sgl_obj_set_bg_img(app, &app_icon);
    sgl_obj_set_alpha(app, 0);
    sgl_appstart_set_text(app, "Alipay");
    sgl_appstart_set_font(app, &song12);

    // static const sgl_style_t msg_style = {.body_color = SGL_CYAN, .text_color = SGL_RED, .radius = 0};
    // msg = sgl_mesgbox_create(main_page, 0);
    // sgl_obj_set_style(msg, &msg_style);
    // sgl_obj_set_size(msg, 80, 15);
    // sgl_obj_set_pos(msg, 20, 180);
    // sgl_mesgbox_set_text(msg, "Alipay");
    // sgl_mesgbox_set_font(msg, &song12);

}
