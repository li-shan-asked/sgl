#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "led.h"
#include "key.h"
#include "lcd.h"
#include "timer.h"
#include "sdram.h"
#include "w25qxx.h"
#include "nand.h"  
#include "sdmmc_sdcard.h"
#include "pcf8574.h"
#include "malloc.h"
#include "ftl.h"  
#include "usbd_msc_core.h"
#include "usbd_usr.h"
#include "usbd_desc.h"
#include "usb_conf.h"
#include "usbd_msc_bot.h"
#include "pcf8574.h"
#include "sgl.h"

/************************************************
 ALIENTEK 阿波罗STM32F7开发板 实验53
 USB读卡器(Slave)实验-HAL库函数版
 技术支持：www.openedv.com
 淘宝店铺：http://eboard.taobao.com 
 关注微信公众平台微信号："正点原子"，免费获取STM32资料。
 广州市星翼电子科技有限公司  
 作者：正点原子 @ALIENTEK
************************************************/

USB_OTG_CORE_HANDLE USB_OTG_dev;
extern vu8 USB_STATUS_REG;		//USB状态
extern vu8 bDeviceState;		//USB连接 情况

sgl_color_t tft_draw_buffer[50 * 480] = {0};

extern const unsigned char icon_test[5000];
static sgl_img_t app_icon = {.bitmap = icon_test, .width = 50, .height = 50,};

 sgl_style_t btn_style = {.body_color = SGL_BLUE, .text_color = SGL_RED, .radius = 50};
 sgl_style_t btn1_style = {.body_color = SGL_GREEN, .text_color = SGL_RED, .radius = 50};
 sgl_style_t btn2_style = {.body_color = SGL_BLACK, .text_color = SGL_RED, .radius = 50};
sgl_obj_t* btn;
sgl_obj_t *main_page;

int stdout_device(const char *str)
{
    printf(str);
    return 0;
}

void tft_disp_area(int16_t x1, int16_t y1, int16_t x2, int16_t y2, const sgl_color_t *src)
{
	LCD_Color_Fill(x1, y1, x2, y2, (uint16_t*)src);
}

sgl_obj_t* sw;
void arcbar_anim_cb(struct sgl_anim *anim, void *data)
{
    static int value = 0;
    if(value > 100) {
        value = 0;
    }
    sgl_arcbar_set_value(anim->obj, value++);
    sgl_obj_event_press(sw);
		if(value%2)
		{
			sgl_obj_event_press(btn);
		}
		else{
			sgl_obj_event_release(btn);
		}
}
void btn_event_cb(sgl_obj_t *obj, void *data)
{
	sgl_button_t *btn = container_of(obj, sgl_button_t, obj);
	static int value = 0;
	if(value)
	{
		btn->toggle_color = SGL_BLACK;
		value = 0;
	}
	else{
		btn->toggle_color = SGL_GREEN;
		value++;
	}
	
}

int main(void)
{
	u8 offline_cnt=0;
	u8 tct=0;
	u8 USB_STA;
	u8 Divece_STA;
    
	Cache_Enable();                 //打开L1-Cache
	HAL_Init();				        //初始化HAL库
	Stm32_Clock_Init(432,25,2,9);   //设置时钟,216Mhz 
	delay_init(216);                //延时初始化
	uart_init(115200);		        //串口初始化
	LED_Init();                     //初始化LED
	KEY_Init();                     //初始化按键
	SDRAM_Init();                   //初始化SDRAM
	LCD_Init();                     //初始化LCD
	W25QXX_Init();				    //初始化W25Q256
	PCF8574_Init();				    //初始化PCF8574 
 	my_mem_init(SRAMIN);		    //初始化内部内存池
	my_mem_init(SRAMEX);		    //初始化外部内存池
	my_mem_init(SRAMDTCM);		    //初始化DTCM内存池 
  POINT_COLOR=RED;
	SD_Init();
	W25QXX_ReadID();
	FTL_Init();
	//MSC_BOT_Data=mymalloc(SRAMIN,MSC_MEDIA_PACKET);			//申请内存
	//USBD_Init(&USB_OTG_dev,USB_OTG_FS_CORE_ID,&USR_desc,&USBD_MSC_cb,&USR_cb);
	delay_ms(1800);			    
	TIM3_Init(10-1,10800-1);
	sgl_device_panel_t tft_panel = {
                                    .framebuffer = tft_draw_buffer,
                                    .buffer_size = sizeof(tft_draw_buffer),
                                    .xres = 480,
                                    .yres = 800,
                                    .xres_virtual = 480,
                                    .yres_virtual = 800,
                                    .flush_area = tft_disp_area
                                    };
	sgl_device_stdout_t stdout_dev = {
																	.put = stdout_device,
																	};


	sgl_device_register(&tft_panel, NULL);

	sgl_device_stdout_register(&stdout_dev);

	sgl_init();
																		
																		
	main_page = sgl_page_create();

	//sgl_obj_set_bg_img(main_page, &bg_img);

	sgl_page_set_active(main_page);

	SGL_LOG_INFO("button demo\r\n");
	//??????,?????SGL_BLUE,?????SGL_RED,?????16
	//????????
	btn = sgl_button_create(main_page);
	//?????????98??32
	sgl_obj_set_size(btn, 200, 100);
	//????????x??100, y??50
	sgl_obj_set_pos(btn, 0, 50);
	//????????btn_style
	sgl_obj_set_style(btn, &btn_style);
	//????????????btn_event,???
  //sgl_obj_set_event_cb(btn, btn_event_cb, NULL);
	//????????????SGL_GREEN
	sgl_button_set_toggle_color(btn, SGL_GREEN);
	//????????song12
	sgl_button_set_font(btn, &song27);
	// //??????????Button
	sgl_button_set_text(btn, "Button");
	
	sgl_obj_t* btn1 = sgl_button_create(main_page);
	//?????????98??32
	sgl_obj_set_size(btn1, 200, 100);
	//????????x??100, y??50
	sgl_obj_set_pos(btn1, 0, 200);
	//????????btn_style
	sgl_obj_set_style(btn1, &btn1_style);
	//????????????btn_event,???
  //sgl_obj_set_event_cb(btn, btn_event_cb, NULL);
	//????????????SGL_GREEN
	sgl_button_set_toggle_color(btn1, SGL_GREEN);
	//????????song12
	sgl_button_set_font(btn1, &song27);
	// //??????????Button
	sgl_button_set_text(btn1, "Button2");
	
	sgl_obj_t* btn2 = sgl_button_create(main_page);
	//?????????98??32
	sgl_obj_set_size(btn2, 200, 100);
	//????????x??100, y??50
	sgl_obj_set_pos(btn2, 280, 200);
	//????????btn_style
	sgl_obj_set_style(btn2, &btn2_style);
	//????????????btn_event,???
  //sgl_obj_set_event_cb(btn, btn_event_cb, NULL);
	//????????????SGL_GREEN
	sgl_button_set_toggle_color(btn2, SGL_GREEN);
	//????????song12
	sgl_button_set_font(btn2, &song27);
	// //??????????Button
	sgl_button_set_text(btn2, "Button3");
	
	sgl_obj_t* btn3 = sgl_button_create(main_page);
	//?????????98??32
	sgl_obj_set_size(btn3, 200, 100);
	//????????x??100, y??50
	sgl_obj_set_pos(btn3, 280, 50);
	//????????btn_style
	sgl_obj_set_style(btn3, &btn_style);
	//????????????btn_event,???
  //sgl_obj_set_event_cb(btn, btn_event_cb, NULL);
	//????????????SGL_GREEN
	sgl_button_set_toggle_color(btn3, SGL_GREEN);
	//????????song12
	sgl_button_set_font(btn3, &song27);
	// //??????????Button
	sgl_button_set_text(btn3, "Button4");


	SGL_LOG_INFO("switch demo\r\n");
	sgl_style_t sw_style = {.body_color = sgl_rgb(10,200,20), .radius = 17};
	
	 //????????,???????
	sw = sgl_switch_create(main_page, true);
	//?????????124??34
	sgl_obj_set_size(sw, 200, 100);
	//??????????????
	sgl_obj_set_pos(sw, 0, 450);
	//???????
	sgl_obj_set_style(sw, &sw_style);
	//?????????
	//sgl_obj_set_event_cb(sw, sw_event, NULL);
	//?????????????
	sgl_switch_set_toggle_color(sw, sgl_rgb(50,50,50));
	//?????????
	sgl_switch_set_status(sw, false);
	sgl_switch_set_anim(sw, true);
	
	
	//??????,?????SGL_BLUE,?????SGL_RED,?????16
	sgl_style_t arcbar_style = {.body_color = SGL_BLUE, .text_color = SGL_RED};
	//???????????
	sgl_obj_t *arcbar = sgl_arcbar_create(main_page, 46);
	//????????????100??100
	sgl_obj_set_size(arcbar, 100, 100);
	//???????????arcbar_style
	sgl_obj_set_style(arcbar, &arcbar_style);
	//???????????x??170, y??120
	sgl_obj_set_pos(arcbar, 300, 450);
	//??????????
	sgl_arcbar_set_font(arcbar, &song27);
	//???????????10
	sgl_arcbar_set_thick(arcbar, 15);

	sgl_anim_t arcbar_anim;
	sgl_anim_set_interval(&arcbar_anim, 10000);
	sgl_anim_set_endless(&arcbar_anim, true);
	sgl_anim_bind_obj(&arcbar_anim, arcbar);
	sgl_anim_set_anim_cb(&arcbar_anim, arcbar_anim_cb, NULL);
	sgl_anim_exec(&arcbar_anim);
		
//	sgl_style_t label_style = {.body_color = SGL_BLUE, .text_color = SGL_RED};
//	sgl_obj_t *lable = sgl_label_create(main_page);
//	sgl_obj_set_size(lable, 200, 100);
//	sgl_obj_set_pos(lable, 0, 600);
//	sgl_obj_set_style(lable, &label_style);
//	sgl_label_set_font(lable, &song27);
//	sgl_label_set_text(lable, "SGL will be better!");
	LCD_ShowString(50,700,400,16,32, "SGL will be better!");
	static i=0;
	while(1)
	{
		sgl_task_handler();
	} 							  	       
}
