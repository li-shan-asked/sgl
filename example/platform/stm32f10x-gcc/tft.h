#include "sgl.h"

void tft_init(void);
void SPI1_Init(void);
void tft_disp_area(int16_t x1, int16_t y1, int16_t x2, int16_t y2, const sgl_color_t *src);
