/*
 * ************************************************
 * 
 *              STM32 blink gcc demo
 * 
 *  CPU: STM32F103C8
 *  PIN: PA1
 * 
 * ************************************************
*/

#include "stm32f10x.h"
#include "misc.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
#include "sgl.h"
#include "tft.h"

#define LED_PERIPH RCC_APB2Periph_GPIOC
#define LED_PORT GPIOC
#define LED_PIN GPIO_Pin_13


void USART1_GPIO_Config(void) {  
    GPIO_InitTypeDef GPIO_InitStructure;  
  
    // 使能GPIOA和USART1时钟  
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_USART1, ENABLE);  
  
    // USART1_TX   GPIOA.9  
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;  
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; // 复用推挽输出  
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;  
    GPIO_Init(GPIOA, &GPIO_InitStructure);  
  
    // USART1_RX   GPIOA.10  
    // 注意：对于发送，我们不需要配置USART1_RX的GPIO  
    // GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;  
    // GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING; // 浮空输入  
    // GPIO_Init(GPIOA, &GPIO_InitStructure);  
}  

// USART1 配置  
void USART1_Config(void) {  
    USART_InitTypeDef USART_InitStructure;  
    // USART1参数初始化  
    USART_InitStructure.USART_BaudRate = 115200; // 波特率115200
    USART_InitStructure.USART_WordLength = USART_WordLength_8b; // 数据位8  
    USART_InitStructure.USART_StopBits = USART_StopBits_1; // 停止位1  
    USART_InitStructure.USART_Parity = USART_Parity_No; // 无校验位  
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None; // 无硬件流控制  
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx; // 收发模式  
  
    // 初始化USART1  
    USART_Init(USART1, &USART_InitStructure);  
  
    // 使能USART1接收和发送中断  
    // 注意：如果你不使用中断，可以注释掉这两行  
    // USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);  
    // USART_ITConfig(USART1, USART_IT_TXE, ENABLE);  
  
    // 使能USART1  
    USART_Cmd(USART1, ENABLE);  
}  
 
void UART1_SendString(const char *str) {
    while (*str != '\0') {
        // 等待发送寄存器为空
        while ((USART1->SR & USART_SR_TXE) == 0);
        // 写入字符到发送寄存器
        USART1->DR = (uint8_t)(*str++);
    }
    // 等待最后一个字符发送完成
    while ((USART1->SR & USART_SR_TC) == 0);
}


void delay(int x)
{
    for (int i = 0; i < x; i++)
    {
        for (int j = 0; j < 1000; j++)
            __NOP();
    }
}

sgl_color_t tft_draw_buffer[20 * 240] = {0};

int stdout_device(const char *str)
{
    UART1_SendString(str);
    return 0;
}

sgl_obj_t* sw;
static sgl_obj_t *msg;
void arcbar_anim_cb(struct sgl_anim *anim, void *data)
{
    static int value = 0;
    if(value > 100) {
        value = 0;
    }
    sgl_arcbar_set_value(anim->obj, value++);
    sgl_obj_event_press(sw);
    //sgl_mesgbox_exit(msg);
}


extern const unsigned char icon_test[5000];
static sgl_img_t app_icon = {.bitmap = icon_test, .width = 50, .height = 50,};

const sgl_style_t btn_style = {.body_color = SGL_BLUE, .text_color = SGL_RED, .radius = 16};
sgl_obj_t* btn;
sgl_obj_t *main_page;


#define SYSCLK_FREQ_72MHz  72000000

void TIM2_Config(void)  
{  
    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;  
    NVIC_InitTypeDef NVIC_InitStructure;  
  
    // 使能TIM2时钟  
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);  
  
    // 配置NVIC中断  
    NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;  
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;  
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;  
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;  
    NVIC_Init(&NVIC_InitStructure);  
  
    // 设置TIM2时间基础单元  
    TIM_TimeBaseStructure.TIM_Period = (SYSCLK_FREQ_72MHz / 8000) - 1;  // 计数到8000-1为500us  
    TIM_TimeBaseStructure.TIM_Prescaler = 71;                       // 预分频器设置，使TIM2的计数器时钟为9kHz (72MHz / (7199 + 1))  
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;  
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  
  
    // 初始化TIM2定时器  
    TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);  
  
    // 使能TIM2的更新中断  
    TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);  
  
    // 启动TIM2定时器  
    TIM_Cmd(TIM2, ENABLE);  
}  
  
// TIM2中断服务函数  
void TIM2_IRQHandler(void)  
{  
    if (TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET)  
    {
        // 清除TIM2更新中断标志  
        TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
        // 在这里添加你的中断处理代码
        sgl_tick_inc(5);
    }
}


int main()
{
    GPIO_InitTypeDef gpioDef;
    RCC_APB2PeriphClockCmd(LED_PERIPH, ENABLE);
    gpioDef.GPIO_Mode = GPIO_Mode_Out_PP;
    gpioDef.GPIO_Pin = LED_PIN;
    gpioDef.GPIO_Speed = GPIO_Speed_10MHz;
    GPIO_Init(LED_PORT, &gpioDef);

    USART1_GPIO_Config();
    USART1_Config();
    TIM2_Config();
    UART1_SendString("STM32F103C8T6 SGL DEMO\r\n");

    

    sgl_device_panel_t tft_panel = {
                                    .framebuffer = tft_draw_buffer,
                                    .buffer_size = sizeof(tft_draw_buffer),
                                    .xres = 240,
                                    .yres = 240,
                                    .xres_virtual = 240,
                                    .yres_virtual = 240,
                                    .flush_area = tft_disp_area
                                    };
    sgl_device_stdout_t stdout_dev = {
                                    .put = stdout_device,
                                    };


    sgl_device_register(&tft_panel, NULL);

    sgl_device_stdout_register(&stdout_dev);

    sgl_init();

    SPI1_Init();
    tft_init();
    


    main_page = sgl_page_create();

    //sgl_obj_set_bg_img(main_page, &bg_img);

    sgl_page_set_active(main_page);

    SGL_LOG_INFO("button demo\r\n");
    //定义一个样式，主体颜色为SGL_BLUE，文本颜色为SGL_RED，圆角半径为16
    //创建一个按钮对象
    btn = sgl_button_create(main_page);
    //设置按钮的尺寸为宽98和高32
    sgl_obj_set_size(btn, 98, 32);
    //设置按钮的位置为x坐标100, y坐标50
    sgl_obj_set_pos(btn, 100, 50);
    //设置按钮的样式为btn_style
    sgl_obj_set_style(btn, &btn_style);
    //设置按钮的事件回调函数为btn_event，参数无
   // sgl_obj_set_event_cb(btn, btn_event, NULL);
    //设置按钮的按下后的颜色为SGL_GREEN
    sgl_button_set_toggle_color(btn, SGL_GREEN);
    //设置按钮的字体为song12
    sgl_button_set_font(btn, &song23);
    // //设置按钮的显示文本为Button
    sgl_button_set_text(btn, "Button");


    SGL_LOG_INFO("switch demo\r\n");
    sgl_style_t sw_style = {.body_color = sgl_rgb(10,200,20), .radius = 17};
    
     //创建一个开关对象，默认是开启状态
    sw = sgl_switch_create(main_page, true);
    //设置开关的大小为宽124和高34
    sgl_obj_set_size(sw, 80, 34);
    //设置开关的位置为中心向左对齐
    sgl_obj_set_pos_align(sw, SGL_ALIGN_CENTER_LEFT);
    //设置开关的样式
    sgl_obj_set_style(sw, &sw_style);
    //设置开关的点击事件
    //sgl_obj_set_event_cb(sw, sw_event, NULL);
    //设置开关的关闭状态下的颜色
    sgl_switch_set_toggle_color(sw, sgl_rgb(50,50,50));
    //设置开关的初始状态
    sgl_switch_set_status(sw, false);
    sgl_switch_set_anim(sw, true);


    //定义一个样式，主体颜色为SGL_BLUE，文本颜色为SGL_RED，圆角半径为16
    sgl_style_t arcbar_style = {.body_color = SGL_BLUE, .text_color = SGL_RED};
    //创建一个弧形指示器对象
    sgl_obj_t *arcbar = sgl_arcbar_create(main_page, 46);
    //设置弧形指示器的大小为宽100和高100
    sgl_obj_set_size(arcbar, 55, 55);
    //设置弧形指示器的样式为arcbar_style
    sgl_obj_set_style(arcbar, &arcbar_style);
    //设置弧形指示器的位置为x坐标170, y坐标120
    sgl_obj_set_pos(arcbar, 120, 120);
    //设置弧形指示器的字体
    sgl_arcbar_set_font(arcbar, &song23);
    //设置弧形指示器的厚度为10
    sgl_arcbar_set_thick(arcbar, 5);

    sgl_anim_t arcbar_anim;
    sgl_anim_set_interval(&arcbar_anim, 1000);
    sgl_anim_set_endless(&arcbar_anim, true);
    sgl_anim_bind_obj(&arcbar_anim, arcbar);
    sgl_anim_set_anim_cb(&arcbar_anim, arcbar_anim_cb, NULL);
    sgl_anim_exec(&arcbar_anim);


    sgl_style_t app_style = {.body_color = SGL_BLUE, .text_color = SGL_RED, .radius = 10};
    sgl_obj_t *app = sgl_appstart_create(main_page);
    sgl_obj_set_style(app, &app_style);
    sgl_obj_set_size(app, 46, 46);
    sgl_obj_set_pos(app, 20, 160);
    sgl_obj_set_bg_img(app, &app_icon);
    sgl_obj_set_alpha(app, 0);
    sgl_appstart_set_text(app, "APP");
    sgl_appstart_set_font(app, &song12);


    static const sgl_style_t msg_style = {.body_color = SGL_BLUE, .text_color = SGL_RED, .radius = 10};
    msg = sgl_mesgbox_create(main_page, 0);
    sgl_obj_set_style(msg, &msg_style);
    sgl_obj_set_size(msg, 20, 20);
    sgl_obj_set_pos(msg, 30, 180);
    sgl_mesgbox_set_text(msg, "Alipay");
    sgl_mesgbox_set_font(msg, &song12);

    while (1)
    {
        // GPIO_WriteBit(LED_PORT, LED_PIN, 0);
        // delay(5000);
        // GPIO_WriteBit(LED_PORT, LED_PIN, 1);
        // delay(5000);
        //sgl_tick_inc(5);
        sgl_task_handler();
    }
}
