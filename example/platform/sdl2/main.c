#include <windows.h> // Windows头文件
#include <stdio.h>
#include "SDL.h"
#include <stdlib.h>
#include <time.h>
#include "../../../source/sgl.h"
#include "./background.h"


SDL_Event myEvent; //SDL事件

SDL_Window * m_window = NULL;
SDL_Renderer * m_renderer = NULL;


#define FRAME_BUFFER_XRES   240
#define FRAME_BUFFER_YRES   320
#define FRAME_BUFFER_XRES_VIRTUAL   240
#define FRAME_BUFFER_YRES_VIRTUAL   320

sgl_color_t sdl2_frame_buffer[FRAME_BUFFER_XRES_VIRTUAL * FRAME_BUFFER_YRES_VIRTUAL] = {0};

sgl_color_t sdl2_draw_buffer[15 * FRAME_BUFFER_XRES] = {0};

static int sdl_show_windows(SDL_Window **m_window, SDL_Renderer **m_renderer)
{
    if ( SDL_CreateWindowAndRenderer( FRAME_BUFFER_XRES_VIRTUAL, FRAME_BUFFER_YRES_VIRTUAL, SDL_WINDOW_SHOWN,
                                      &(*m_window), &(*m_renderer )) != 0 )
        return -1;

    // Clear the window with a black background
    for(int i = 0; i< FRAME_BUFFER_XRES * FRAME_BUFFER_YRES; i++) {
        sdl2_frame_buffer[i] = SGL_WHITE;
    }
    SDL_SetRenderDrawColor( *m_renderer, 0, 0, 0, 255);
    SDL_RenderClear( *m_renderer );
    return 0;
}

static void sgl_disp_pixel(int16_t x, int16_t y, sgl_color_t color)
{
    sdl2_frame_buffer[y*FRAME_BUFFER_XRES_VIRTUAL + x] =  color;
}


static void sdl2_disp_area(int16_t x1, int16_t y1, int16_t x2, int16_t y2, const sgl_color_t *src)
{
    const sgl_color_t *color = src;
    int16_t x = x1, y = y1, hight = y2 - y1 + 1, width = x2 - x1 + 1;
    for(int i = y; i < (hight + y); i++ )
    {
        for(int j = x; j < (width + x) ; j ++ )
        {
           sgl_disp_pixel(j, i, *color);
           color ++;
        }
    }
}

static void update_win(SDL_Renderer * m_renderer)
{
    SDL_Texture *texture = SDL_CreateTexture(m_renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, FRAME_BUFFER_XRES_VIRTUAL, FRAME_BUFFER_YRES_VIRTUAL);
    SDL_UpdateTexture(texture, NULL, sdl2_frame_buffer, FRAME_BUFFER_XRES_VIRTUAL * 4);
    SDL_RenderCopy(m_renderer, texture, NULL, NULL);
    SDL_RenderPresent(m_renderer);
}


Uint32 fb_update(Uint32 interval, void *param)//回调函数 
{
	update_win(m_renderer);
	return interval;
}

//sgl task定时器
Uint32 sgl_system_tick(Uint32 interval, void *param)//回调函数 
{
	sgl_tick_inc(5);
    sgl_task_handler();
	return interval;
}

sgl_event_pos_t sdl_mouse_get(void *data)
{
    sgl_event_pos_t ev_pos;
    SDL_Event *event = (SDL_Event*)data;
    switch (event->type)
    {
    case SDL_MOUSEBUTTONDOWN:
        ev_pos.x = event->motion.x;
        ev_pos.y = event->motion.y;
        ev_pos.type = SGL_EVENT_PRESSED;
        break;
    case SDL_MOUSEBUTTONUP:
        ev_pos.x = event->motion.x;
        ev_pos.y = event->motion.y;
        ev_pos.type = SGL_EVENT_RELEASED;
        break;
    default: ev_pos.type = SGL_EVENT_NULL;
    }
    return ev_pos;
}

// 鼠标中断处理函数
int sgl_mouse_event_interrupt(void *userdata, SDL_Event *event) 
{
    sgl_device_input_handle(event);
    return 0;
}

int stdout_device(const char *str)
{
    return printf("%s\n", str);
}


extern void demo_setup(void);

int main( int argc, char * argv[] )
{
    int quit = 0;
    if ( SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) != 0 )
        return -1;
    sdl_show_windows(&m_window, &m_renderer);

    sgl_device_panel_t sdl_panel = {
                                    .framebuffer = sdl2_frame_buffer,
                                    //.framebuffer = sdl2_draw_buffer,
                                    .buffer_size = sizeof(sdl2_draw_buffer),
                                    .xres = FRAME_BUFFER_XRES,
                                    .yres = FRAME_BUFFER_YRES,
                                    .xres_virtual = FRAME_BUFFER_XRES_VIRTUAL,
                                    .yres_virtual = FRAME_BUFFER_YRES_VIRTUAL,
                                    .flush_area = sdl2_disp_area
                                    };
    
    sgl_device_input_t sdl_mouse = {
                                    .get = sdl_mouse_get,
                                   };

    sgl_device_stdout_t stdout_dev = {
                                    .put = stdout_device,
                                    };

    sgl_device_register(&sdl_panel, &sdl_mouse);
    
    sgl_device_stdout_register(&stdout_dev);

    sgl_init();

    SGL_LOG_INFO("start SDL2 sgl...");

    demo_setup();

    SDL_TimerID timer = SDL_AddTimer(50, fb_update, NULL);//创立定时器timer
    SDL_TimerID timer_tick = SDL_AddTimer(5, sgl_system_tick, NULL);//创立定时器timer
    (void)timer_tick; //clear warning
    SDL_AddEventWatch(sgl_mouse_event_interrupt, NULL);
    while (!quit)
    {
        SDL_WaitEvent(&myEvent);
        switch (myEvent.type)
        {
                        
        case SDL_QUIT:
            quit = 1;
            break;
        }
    }
	SDL_RemoveTimer(timer);
    SDL_DestroyWindow( m_window );
    SDL_DestroyRenderer( m_renderer );
    SDL_Quit();
    return 1;
}
