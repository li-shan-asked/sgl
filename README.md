# SGL (Small Graphics Library)
![SGL_LOGO](SGL_logo.png)
  
#### 介绍
SGL (Small Graphics Library)是一种轻量快速的图形库，该库旨在为MCU级别的处理器提供一美观轻量的GUI（Graphics User Interface).
文档参考地址：www.sgl-io.cn

## How to build it
1. to install gcc compiler
2. cd example/demo
3. make -f sdl.mk
4. ./xxx.exe

注意: 请使用git终端或者VSCode下面的git bash终端输入上面命令

## 1. To install gcc compiler
you can download it from this: https://github.com/niXman/mingw-builds-binaries/releases/download/13.2.0-rt_v11-rev0/x86_64-13.2.0-release-posix-seh-ucrt-rt_v11-rev0.7z   
then, you need to decompression it, and add environment variable into Windows10's system `PATH`   
   
## 2. get `sgl` source code
clone source code on you windows OS and change directory to source `example/demo`: 

git clone https://gitee.com/li-shan-asked/sgl.git

cd example/demo

## 3. make it
run `make` command, and it will be built to `xxx.exe`

   
## 4. run it
run `./xxx.exe`
